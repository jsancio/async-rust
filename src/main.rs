#![feature(async_await)]

use futures::executor::enter;
use futures::future::LocalFutureObj;
use futures::ready;
use futures::stream::FuturesUnordered;
use futures::stream::StreamExt;
use futures::task::noop_waker;
use futures::task::LocalSpawn;
use futures::task::SpawnError;
use miow::iocp::CompletionPort;
use miow::net::TcpStreamExt;
use miow::Overlapped;
use slab::Slab;
use std::cell::RefCell;
use std::convert::TryInto;
use std::future::Future;
use std::io::Error;
use std::io::ErrorKind;
use std::io::Read;
use std::io::Result as IoResult;
use std::io::Write;
use std::mem::replace;
use std::net::TcpListener;
use std::net::TcpStream;
use std::net::ToSocketAddrs;
use std::os::windows::io::AsRawSocket;
use std::pin::Pin;
use std::rc::Rc;
use std::rc::Weak;
use std::task::Context;
use std::task::Poll;
use std::task::Waker;
use std::thread;
use std::time::Duration;
use winapi::um::minwinbase::OVERLAPPED;
use winapi::um::winnt::HANDLE;

fn main() -> IoResult<()> {
    async_main()
}

fn async_main() -> IoResult<()> {
    let addr = "127.0.0.1:6767";
    let server_thread = thread::spawn(move || {
        let listener = TcpListener::bind(addr).expect("error binding to address");
        let (mut peer1, _) = listener
            .accept()
            .expect("error accepting client connection");

        let (mut peer2, _) = listener.accept().expect("error accepting peer connection");

        let mut count = 0;
        thread::sleep(Duration::from_secs(5));
        println!("Done sleeping in reader thread");
        let mut buffer = [0; 1];
        while peer1
            .read(&mut buffer)
            .expect("error reading bytes to client")
            != 0
        {
            assert_eq!(buffer, [54; 1]);
            let size = peer2.write(&buffer).expect("error writing bytes to client");
            count += size;
        }

        println!("Finished writing {} bytes to peer", count);
    });

    let mut event_loop = EventLoop::new()?;
    let peer1 = AsyncTcpStream::connect(event_loop.registry(), addr)?;
    let buffer = vec![54; 1024 * 1024];

    let peer2 = AsyncTcpStream::connect(event_loop.registry(), addr)?;

    event_loop
        .spawner()
        .spawn_local_obj(From::from(Box::new(WriteFuture::new(peer1, buffer))))
        .map_err(|err| {
            dbg!(err);
            Error::new(ErrorKind::Other, "Got a spawning error")
        })?;

    event_loop
        .spawner()
        .spawn_local_obj(From::from(Box::new(ReadFuture::new(peer2))))
        .map_err(|err| {
            dbg!(err);
            Error::new(ErrorKind::Other, "Got a spawning error")
        })?;

    event_loop.run()?;

    server_thread.join().expect("error joining thread");

    Ok(())
}

struct ReadFuture {
    peer: AsyncTcpStream,
    count: usize,
}

impl ReadFuture {
    pub fn new(peer: AsyncTcpStream) -> Self {
        ReadFuture { peer, count: 0 }
    }
}

impl Future for ReadFuture {
    type Output = ();

    fn poll(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        loop {
            let mut buffer = match ready!(this.peer.poll_read_ready(ctx)) {
                Ok(Some(mut buffer)) => {
                    println!(
                        "poll_read_ready buffer: len = {}, first = {:?}, last = {:?}",
                        buffer.len(),
                        buffer.first(),
                        buffer.last()
                    );
                    if buffer.len() == 0 {
                        return Poll::Ready(());
                    }

                    this.count += buffer.len();
                    println!("Total read bytes: {}", this.count);

                    buffer.clear();
                    buffer.resize(buffer.capacity(), 0);
                    buffer
                }
                Ok(None) => {
                    println!("First time reading create a buffer");
                    vec![0; 1024 * 1024]
                }
                Err(err) => {
                    // TODO: fix me please! we should be handling error by returning it
                    dbg!(err);
                    panic!("Got an error reading from the peer!");
                }
            };

            loop {
                println!("Asking to read some bytes");
                buffer = match ready!(this.peer.poll_read(ctx, buffer)) {
                    Ok(mut buffer) => {
                        println!(
                            "poll_read buffer: len = {}, first = {:?}, last = {:?}",
                            buffer.len(),
                            buffer.first(),
                            buffer.last()
                        );
                        if buffer.len() == 0 {
                            return Poll::Ready(());
                        }

                        this.count += buffer.len();
                        println!("Total read bytes: {}", this.count);

                        buffer.clear();
                        buffer.resize(buffer.capacity(), 0);
                        buffer
                    }
                    Err(err) => {
                        // TODO: fix me please! we should be handling error by returning it
                        dbg!(err);
                        panic!("Got an error reading from the peer!");
                    }
                };
            }
        }
    }
}

struct WriteFuture {
    peer: AsyncTcpStream,
    buffer: Option<Vec<u8>>,
}

impl WriteFuture {
    pub fn new(peer: AsyncTcpStream, buffer: Vec<u8>) -> Self {
        WriteFuture {
            peer,
            buffer: Some(buffer),
        }
    }
}

impl Future for WriteFuture {
    type Output = ();

    fn poll(self: Pin<&mut Self>, ctx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();

        if let Some(buffer) = this.buffer.take() {
            // TODO: This is a hack. Implement this for real!!!
            loop {
                // This clone is okay because this is just test code.
                match ready!(this.peer.poll_write(ctx, buffer.clone())) {
                    Ok(_) => {
                        // TODO: It is okay to drop the buffer because this is test code
                        println!("Write complete synchronously, continue writing.");
                    }
                    Err(err) => {
                        // TODO: fix me please! we should be handling error by returning it
                        dbg!(err);
                        panic!("Got an error  that we don't know how to handle!");
                    }
                }
            }
        }

        match ready!(this.peer.poll_complete(ctx)) {
            Ok(Some(result)) => {
                println!("We got a completion: {}", result.len());
                Poll::Ready(())
            }
            result => {
                println!("We got a resut: {:?}", result);
                Poll::Ready(())
            }
        }
    }
}

#[derive(Debug)]
enum OpState {
    Waker(Waker),
    Transferred(usize),
    Idle,
}

struct Operation {
    overlapped: Box<Overlapped>,
    state: OpState,
}

impl Default for Operation {
    fn default() -> Self {
        Operation {
            overlapped: Box::new(Overlapped::zero()),
            state: OpState::Idle,
        }
    }
}

struct Task {
    write_state: Operation,
    read_state: Operation,
}

impl Task {
    fn new() -> Task {
        Task {
            write_state: Default::default(),
            read_state: Default::default(),
        }
    }
}

struct Inner {
    completion_port: CompletionPort,
    slab: RefCell<Slab<Task>>,
}

type Incoming = RefCell<Vec<LocalFutureObj<'static, ()>>>;

struct EventLoop {
    inner: Rc<Inner>,
    pool: FuturesUnordered<LocalFutureObj<'static, ()>>,
    incoming: Rc<Incoming>,
}

impl EventLoop {
    pub fn new() -> IoResult<EventLoop> {
        let completion_port = CompletionPort::new(1)?;
        Ok(EventLoop {
            inner: Rc::new(Inner {
                completion_port,
                slab: Default::default(),
            }),
            pool: Default::default(),
            incoming: Default::default(),
        })
    }

    pub fn spawner(&self) -> LocalSpawner {
        LocalSpawner {
            incoming: Rc::downgrade(&self.incoming),
        }
    }

    pub fn registry(&self) -> Registry {
        Registry {
            inner: Rc::downgrade(&self.inner),
        }
    }

    // TODO: this function looks like an exact duplicate of what we implemented for Linux
    fn poll_pool(&mut self, ctx: &mut Context<'_>) -> Poll<()> {
        loop {
            for task in self.incoming.borrow_mut().drain(..) {
                self.pool.push(task)
            }

            let result = self.pool.poll_next_unpin(ctx);

            if self.incoming.borrow().is_empty() {
                match result {
                    Poll::Pending => break Poll::Pending,
                    Poll::Ready(None) => break Poll::Ready(()),
                    Poll::Ready(Some(())) => {}
                }
            }
        }
    }

    fn wait_on_events(&mut self) -> IoResult<()> {
        // TODO: refactor this so we can implement deplay events

        let status = self.inner.completion_port.get(None)?;

        match self.inner.slab.borrow_mut().get_mut(status.token()) {
            Some(task) => {
                if status.overlapped() == task.write_state.overlapped.raw() {
                    // TODO: This code looks very similar to the code below
                    let size: usize = status.bytes_transferred().try_into().unwrap();
                    match replace(&mut task.write_state.state, OpState::Transferred(size)) {
                        OpState::Waker(waker) => waker.wake(),
                        other => {
                            task.write_state.state = other;
                            panic!(
                                "Reactor got an write event for {} but no waker registered: {:?}",
                                status.token(),
                                task.write_state.state
                            );
                        }
                    }
                } else if status.overlapped() == task.read_state.overlapped.raw() {
                    // TODO: This code looks very similar to the code above

                    let size: usize = status.bytes_transferred().try_into().unwrap();
                    match replace(&mut task.read_state.state, OpState::Transferred(size)) {
                        OpState::Waker(waker) => waker.wake(),
                        other => {
                            task.read_state.state = other;
                            panic!(
                                "Reactor got an read event for {} but no waker registered: {:?}",
                                status.token(),
                                task.read_state.state
                            );
                        }
                    }
                }
            }
            None => {
                // TODO: this looks like a duplicate of similar code in the Linux implementation
                eprintln!("Reactor got an event for a file which is not registered")
            }
        }

        Ok(())
    }

    // TODO: this function looks like an exact duplicate of what we implemented for Linux
    pub fn run(&mut self) -> IoResult<()> {
        let _enter = enter().expect("Cannot execute event loop from within another event loop");
        let waker = noop_waker();
        let mut context = Context::from_waker(&waker);

        loop {
            if self.poll_pool(&mut context) == Poll::Ready(()) {
                break Ok(());
            }

            self.wait_on_events()?;
        }
    }
}

// TODO: this should be platform independent!!!
pub struct LocalSpawner {
    incoming: Weak<Incoming>,
}

impl LocalSpawn for LocalSpawner {
    fn spawn_local_obj(&mut self, future: LocalFutureObj<'static, ()>) -> Result<(), SpawnError> {
        if let Some(incoming) = self.incoming.upgrade() {
            incoming.borrow_mut().push(future);
            Ok(())
        } else {
            Err(SpawnError::shutdown())
        }
    }

    fn status_local(&self) -> Result<(), SpawnError> {
        if self.incoming.upgrade().is_some() {
            Ok(())
        } else {
            Err(SpawnError::shutdown())
        }
    }
}

pub struct Registry {
    inner: Weak<Inner>,
}

impl Registry {
    pub(crate) fn add_socket(
        &mut self,
        stream: &TcpStream,
    ) -> IoResult<(usize, *mut OVERLAPPED, *mut OVERLAPPED)> {
        match self.inner.upgrade() {
            Some(inner) => {
                let task = Task::new();
                let write_overlapped = task.write_state.overlapped.raw();
                let read_overlapped = task.read_state.overlapped.raw();
                let token = inner.slab.borrow_mut().insert(task);
                match inner.completion_port.add_socket(token, stream) {
                    Err(err) => {
                        inner.slab.borrow_mut().remove(token);
                        return Err(err);
                    }
                    Ok(()) => {}
                };

                Ok((token, write_overlapped, read_overlapped))
            }

            None => Err(Error::new(ErrorKind::Other, "Event lopp was closed")),
        }
    }

    // TODO: How do we get errors? Instead of inner having write_size it should be a
    // pending result of type IoResult<usize>
    pub(crate) fn write_size(&mut self, token: usize) -> Poll<IoResult<Option<usize>>> {
        match self.inner.upgrade() {
            Some(inner) => match inner.slab.borrow_mut().get_mut(token) {
                Some(task) => match task.write_state.state {
                    OpState::Waker(_) => Poll::Pending,
                    OpState::Transferred(size) => {
                        task.write_state.state = OpState::Idle;
                        Poll::Ready(Ok(Some(size)))
                    }
                    OpState::Idle => Poll::Ready(Ok(None)),
                },
                None => Poll::Ready(Err(Error::new(
                    ErrorKind::Other,
                    format!("Task for token {} was not registered", token),
                ))),
            },
            None => Poll::Ready(Err(Error::new(ErrorKind::Other, "Event loop was closed"))),
        }
    }

    pub(crate) fn read_size(&mut self, token: usize) -> Poll<IoResult<Option<usize>>> {
        match self.inner.upgrade() {
            Some(inner) => match inner.slab.borrow_mut().get_mut(token) {
                Some(task) => match task.read_state.state {
                    OpState::Waker(_) => Poll::Pending,
                    OpState::Transferred(size) => {
                        task.read_state.state = OpState::Idle;
                        Poll::Ready(Ok(Some(size)))
                    }
                    OpState::Idle => Poll::Ready(Ok(None)),
                },
                None => Poll::Ready(Err(Error::new(
                    ErrorKind::Other,
                    format!("Task for token {} was not registered", token),
                ))),
            },
            None => Poll::Ready(Err(Error::new(ErrorKind::Other, "Event loop was closed"))),
        }
    }

    pub(crate) fn subscribe_write(&mut self, token: usize, ctx: &mut Context<'_>) -> IoResult<()> {
        self.inner
            .upgrade()
            .ok_or(Error::new(ErrorKind::Other, "Event loop was shutdown"))
            .and_then(|inner| {
                inner
                    .slab
                    .borrow_mut()
                    .get_mut(token)
                    .ok_or(Error::new(
                        ErrorKind::Other,
                        format!("Task not found for token {}.", token),
                    ))
                    .map(|task| {
                        // TODO: We should check the state of the system. Can only do this when it
                        // is Idle
                        task.write_state.state = OpState::Waker(ctx.waker().clone());
                    })
            })
    }

    pub(crate) fn subscribe_read(&mut self, token: usize, ctx: &mut Context<'_>) -> IoResult<()> {
        self.inner
            .upgrade()
            .ok_or(Error::new(ErrorKind::Other, "Event loop was shutdown"))
            .and_then(|inner| {
                inner
                    .slab
                    .borrow_mut()
                    .get_mut(token)
                    .ok_or(Error::new(
                        ErrorKind::Other,
                        format!("Task not found for token {}.", token),
                    ))
                    .map(|task| {
                        // TODO: We should check the state of the system. Can only do this when it
                        // is Idle
                        task.read_state.state = OpState::Waker(ctx.waker().clone());
                    })
            })
    }
}

trait AsyncWrite {
    fn poll_write_ready(&mut self, ctx: &mut Context<'_>) -> Poll<IoResult<Option<Vec<u8>>>>;
    fn poll_write(&mut self, ctx: &mut Context<'_>, buffer: Vec<u8>) -> Poll<IoResult<Vec<u8>>>;
    fn poll_complete(&mut self, ctx: &mut Context<'_>) -> Poll<IoResult<Option<Vec<u8>>>>;
}

trait AsyncRead {
    fn poll_read_ready(&mut self, ctx: &mut Context<'_>) -> Poll<IoResult<Option<Vec<u8>>>>;
    fn poll_read(&mut self, ctx: &mut Context<'_>, buffer: Vec<u8>) -> Poll<IoResult<Vec<u8>>>;
}

struct AsyncTcpStream {
    tcp_stream: TcpStream,
    registry: Registry,
    token: usize,
    write_overlapped: *mut OVERLAPPED,
    write_state: Option<Vec<u8>>,
    read_overlapped: *mut OVERLAPPED,
    read_state: Option<Vec<u8>>,
}

impl AsyncTcpStream {
    pub(crate) fn new(mut registry: Registry, tcp_stream: TcpStream) -> IoResult<AsyncTcpStream> {
        let (token, write_overlapped, read_overlapped) = registry.add_socket(&tcp_stream)?;

        {
            // TODO: think about moving this code somewhere else
            let flags = winapi::um::winbase::FILE_SKIP_SET_EVENT_ON_HANDLE
                | winapi::um::winbase::FILE_SKIP_COMPLETION_PORT_ON_SUCCESS;
            let r = unsafe {
                winapi::um::winbase::SetFileCompletionNotificationModes(
                    tcp_stream.as_raw_socket() as HANDLE,
                    flags,
                )
            };
            if r != winapi::shared::minwindef::TRUE {
                return Err(Error::last_os_error());
            }
        }

        Ok(AsyncTcpStream {
            tcp_stream,
            registry,
            token,
            write_overlapped,
            write_state: Default::default(),
            read_overlapped,
            read_state: Default::default(),
        })
    }

    pub fn connect<T>(registry: Registry, addrs: T) -> IoResult<AsyncTcpStream>
    where
        T: ToSocketAddrs,
    {
        AsyncTcpStream::new(registry, TcpStream::connect(addrs)?)
    }
}

impl AsyncRead for AsyncTcpStream {
    // The vector return will have a length set of the number of bits read. If the vector is empty
    // it means that the remote connection was closed
    fn poll_read_ready(&mut self, ctx: &mut Context<'_>) -> Poll<IoResult<Option<Vec<u8>>>> {
        if let Some(mut buffer) = self.read_state.take() {
            let size = match dbg!(ready!(self.registry.read_size(self.token))) {
                Ok(size) => size,
                Err(err) => {
                    // TODO: Need to return the error, how much it was written and the buffer
                    return Poll::Ready(Err(err));
                }
            };

            if let Some(size) = size {
                buffer.truncate(size);
                Poll::Ready(Ok(Some(buffer)))
            } else {
                // TODO: We need to look into how we are going to determine that the connection is close. This is usually done by return a size of zero.
                // Perform the actual read operation
                match unsafe {
                    self.tcp_stream
                        .read_overlapped(&mut buffer, self.read_overlapped)
                } {
                    Ok(Some(size)) => {
                        println!("Overlapped read completed poll_read_ready: {}", size);
                        buffer.truncate(size);
                        Poll::Ready(Ok(Some(buffer)))
                    }
                    Ok(None) => {
                        println!("Pending read overlapped in poll_read_ready.");
                        match self.registry.subscribe_read(self.token, ctx) {
                            Err(err) => {
                                // TODO: return the buffer with the error
                                Poll::Ready(Err(err))
                            }
                            Ok(()) => {
                                self.read_state = Some(buffer);
                                Poll::Pending
                            }
                        }
                    }
                    Err(err) => {
                        // TODO: return the buffer with the error
                        Poll::Ready(Err(err))
                    }
                }
            }
        } else {
            Poll::Ready(Ok(None))
        }
    }

    fn poll_read(&mut self, ctx: &mut Context<'_>, buffer: Vec<u8>) -> Poll<IoResult<Vec<u8>>> {
        if self.read_state.is_none() {
            self.read_state = Some(buffer);
            match ready!(self.poll_read_ready(ctx)) {
                Ok(Some(buffer)) => Poll::Ready(Ok(buffer)),
                Err(err) => Poll::Ready(Err(err)),
                Ok(None) => unreachable!(), // Cannot return None because we set Some(buffer)
            }
        } else {
            Poll::Ready(Err(Error::new(
                ErrorKind::Other,
                "Error reading buffer, AsyncTcpStream is full",
            )))
        }
    }
}

impl AsyncWrite for AsyncTcpStream {
    fn poll_write_ready(&mut self, ctx: &mut Context<'_>) -> Poll<IoResult<Option<Vec<u8>>>> {
        if let Some(mut buffer) = self.write_state.take() {
            match dbg!(ready!(self.registry.write_size(self.token))) {
                Ok(Some(size)) => {
                    assert_eq!(
                        size,
                        buffer.len(),
                        "Pending write must equal the size of the buffer"
                    );
                    Poll::Ready(Ok(Some(buffer)))
                }
                Ok(None) => {
                    // Perform the actual write operation
                    match unsafe {
                        self.tcp_stream
                            .write_overlapped(&mut buffer, self.write_overlapped)
                    } {
                        Ok(Some(size)) => {
                            assert_eq!(
                                buffer.len(),
                                size,
                                "The written size did not match the pending buffer size"
                            );

                            println!("Overlapped write completed poll_write_ready: {}", size);
                            Poll::Ready(Ok(Some(buffer)))
                        }
                        Ok(None) => {
                            println!("Pending write overlapped in poll_write_ready.");
                            match self.registry.subscribe_write(self.token, ctx) {
                                Err(err) => {
                                    // TODO: return the buffer with the error
                                    Poll::Ready(Err(err))
                                }
                                Ok(()) => {
                                    self.write_state = Some(buffer);
                                    Poll::Pending
                                }
                            }
                        }
                        Err(err) => {
                            // TODO: return the buffer with the error
                            Poll::Ready(Err(err))
                        }
                    }
                }
                Err(err) => {
                    // TODO: Need to return the error, how much it was written and the buffer
                    return Poll::Ready(Err(err));
                }
            }
        } else {
            Poll::Ready(Ok(None))
        }
    }

    fn poll_write(&mut self, ctx: &mut Context<'_>, buffer: Vec<u8>) -> Poll<IoResult<Vec<u8>>> {
        if self.write_state.is_none() {
            self.write_state = Some(buffer);
            match ready!(self.poll_write_ready(ctx)) {
                Ok(Some(buffer)) => Poll::Ready(Ok(buffer)),
                Err(err) => Poll::Ready(Err(err)),
                Ok(None) => unreachable!(), // Cannot return None because we set Some(buffer)
            }
        } else {
            Poll::Ready(Err(Error::new(
                ErrorKind::Other,
                "Error writing buffer, AsyncTcpStream is full",
            )))
        }
    }

    fn poll_complete(&mut self, ctx: &mut Context<'_>) -> Poll<IoResult<Option<Vec<u8>>>> {
        self.poll_write_ready(ctx)
    }
}
